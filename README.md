# A Person Site #

This is a website template someone can use to make their personal website. The template is privacy-friendly (not many website trackers), accessibility friendly (good color contrast, easy to read font, a good size text, not auto playing or animations), and a functional website.

## Installation

Clone the repo, or download it as a zip, and put it in your folder of choice. Then open your favorite text editor and edit as you see fit. Then copy it to your web server.

## Live Demo
[https://projects.gregoryhammond.ca/apersonsite/](https://projects.gregoryhammond.ca/apersonsite/)

## License
[Unlicense](https://unlicense.org/)